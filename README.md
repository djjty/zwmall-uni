# zwmall-uni

#### 掌沃商城PC前端 - 基于uni-app

#### 项目介绍
掌沃商城（zwmall）是精仿小米官网开发的前后端分离的PC商城

前台预览地址：[掌沃商城](https://zwmall.chengdongqing.top)

后台预览地址：[掌沃商城管理系统](https://admin.zwmall.chengdongqing.top)

在JFinal官网的分享地址：[掌沃商城](https://jfinal.com/share/2127)

在uni-app插件市场的分享地址：[掌沃商城](https://ext.dcloud.net.cn/plugin?id=1431)

#### 源码获取
前台前端：[zwmall-uni](https://gitee.com/chengdongqing/zwmall-uni)

后台前端：[zwmall-admin](https://gitee.com/chengdongqing/zwmall-admin)

后端接口：[zwmall-api](https://gitee.com/chengdongqing/zwmall-api)

#### 项目配置

- 核心框架：uni-app

- HBuilder X 版本：HBuilder X 2.7.14

### 功能亮点
- 首页效果实现，包括闪购、搜索框等
- 搜索页效果实现，包括过滤器、分页条、商品推荐等
- 商品详情页效果实现，包括概述和参数渲染及评论加载、图片懒加载、多规格选择、轮播图切换、动态图片放大镜等
- 数据请求、文件上传方法的统一封装
- 购物车页效果实现，包括计数器、多选、js调用全局alert，以及顶部迷你购物车实现，登录后本地购物车同步到数据库
- 订单的地址选择、请求支付、订单进度条等
- 收货地址的添加、修改
- 头像裁剪组件、日期选择器、城市选择器
- 修改邮箱或手机号中的步骤条、倒计时、图片验证码获取等
- 评价晒单中的支持小数点的评分组件、图片上传等
- 还有若干未提及的技术点
- 整个项目无需第三方插件

#### 项目历程
    本项目是我在疫情期间独立开发的商城项目，源自我在学校的毕业项目。毕业两年多，该项目经过不断优化和不同版本的重构开发，今天终于可以和大家见面了。本项目未经过企业级验证，欢迎大家给我反馈问题及提出建议。
    
    路遥  2020-06-01
    邮箱：1912525497@qq.com
    QQ群：550850198

<img src="https://images.gitee.com/uploads/images/2020/0701/152053_ac7e505c_1499515.jpeg" 
height="800">

#### 项目说明
    测试账号：19999999999
    密码：123456789

    管理系统账号同上，修改及删除数据将被限制。
    
    由于我没有商品闪购相关开发经验，所以闪购这块暂时是静态的。由于缺少微信支付、短信验证码的账号信息，故微信支付和短信验证码暂时无法预览，但相关功能均已通过测验。

    支付宝测试是在沙箱环境，需要用到支付宝沙箱版app，沙箱环境可能会不稳定。
    下载地址：https://sandbox.alipaydev.com/user/downloadApp.htm
    账号：ippgcu2572@sandbox.com    登录密码：111111    支付密码：111111
    可以扫描下方二维码下载支付宝沙箱版APP：
![支付宝沙箱版APP](https://images.gitee.com/uploads/images/2020/0701/162333_8848fa81_1499515.png "支付宝沙箱版APP")