export default [{
		name: '个人中心',
		linkUrl: '/pages/user/index'
	},
	{
		name: '评价晒单',
		linkUrl: '/pages/order/comment/index'
	},
	{
		name: '我的喜欢',
		linkUrl: '/pages/user/favorite/index'
	},
	{
		name: '账户管理',
		linkUrl: '/pages/user/security/index'
	}
]
